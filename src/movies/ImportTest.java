package movies;

import java.io.BufferedReader;

import java.io.File;
import java.sql.*;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

import java.io.InputStreamReader;

public class ImportTest {
	

	static {
		try {Class.forName("com.mysql.jdbc.Driver");
		String dbUrl = "jdbc:mysql://localhost/testing?";
		String username = "root";
		String password = "";
	Connection conn = DriverManager.getConnection(dbUrl, username, password);
		
		conn.createStatement().execute("CREATE TABLE books(\n" + "  id integer primary key auto_increment,\n" + "  book_id varchar(25) not null unique,\n" +
	         "  author varchar(50) not null,\n" + "  title varchar(250) not null,\n" + "  genre varchar(25) not null,\n" +
	         "  price float not null,\n" + "  publish_date date not null,\n" + "  description text not null\n" + ")");
		
		File file = new File(fileName);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document xmlDoc = (Document) builder.parse(file);
		
		XPath xpath = XPathFactory.newInstance().newXPath();
		Object res = xpath.evaluate("/numberouno/LieneTest", xmlDoc,XPathConstants.NODESET);
		
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO books(\n" + "  book_id, author, title, genre, price,\n" +
                "  publish_date, description)\n" + "VALUES(?, ?, ?, ?, ?,\n" + "  str_to_date(?, '%Y-%m-%d'), ?)");
		
	
		
		} catch (ClassNotFoundException | SQLException ex) {
			System.err.println("Driver not found: " + ex.getMessage());
		}
	}
	

	static private String getAttrValue(Node node,String attrName) {
	    if ( ! node.hasAttributes() ) return "";
	    NamedNodeMap nmap = node.getAttributes();
	    if ( nmap == null ) return "";
	    Node n = nmap.getNamedItem(attrName);
	    if ( n == null ) return "";
	    return n.getNodeValue();
	    
	    
	    
	}
	
	static private String getTextContent(Node parentNode,String childName) {
	    NodeList nlist = parentNode.getChildNodes();
	    for (int i = 0 ; i < nlist.getLength() ; i++) {
	    Node n = nlist.item(i);
	    String name = n.getNodeName();
	    if ( name != null && name.equals(childName) )
	        return n.getTextContent();
	    }
	    return "";
	
	for (int i = 0 ; i < nlist.getLength() ; i++) {
	    Node node = nlist.item(i);
	    List<String> columns = Arrays.asList(getAttrValue(node, "id"),
	        getTextContent(node, "author"),
	        getTextContent(node, "title"),
	        getTextContent(node, "genre"),
	        getTextContent(node, "price"),
	        getTextContent(node, "publish_date"),
	        getTextContent(node, "description"));
	    for (int n = 0 ; n < columns.size() ; n++) {
	    stmt.setString(n+1, columns.get(n));
	   } stmt.execute();
	}
	
}
